
#RDM JSON Generator

## Description
The RDM JSON Generator is is a Java tool developed for generating the JSON files for the LookUps. It to converts data stored in a flat or csv format into a JSON format ready to be consumed by the RDM Dataload Tool. The tool is exceptionally useful in circumstances where you are attempting to load lookupTypes  and  lookup data coming from different sources. A configuration file and mapping file must be created to be used as parameters.

##Change Log

```
#!plaintext
Last Update Date: 13/09/2021
Version: 3.3.4
Description: enabled flag at Canonical value level, Localizations with Language Code, Value and Description can now be passed via the mapping file.


#!plaintext
Last Update Date: 08/09/2021
Version: 3.3.3
Description: startDate and endDate values can now be passed in Epoch milliseconds format, along with Enabled, Canonical Value and Downstream default value flags and Description field.


#!plaintext
Last Update Date: 23/09/2019
Version: 3.3.1
Description: Standardizing Jar name, Standardizing jar verion


#!plaintext
Last Update Date: 27/06/2019
Version: 3.3
Description: Standardizing Jar name, Changed group name and Upgraded the Reltio CST Core Version


#!plaintext
Last Update Date: 28/03/2019
Version: 3.2
Description: Worked on Clear validation message when properties are missing


Last Update Date: 12/11/2018
Version: 3.1
Description: Standarization of Logs to Log4j2, Remove unused dependency

Last Update Date: 03/08/2018
Version: 2.1.0
Description: Upgraded the Reltio CST Core Version. Renamed INPUT_DATA_FILE_PATH to INPUT_DATA_FILE, renamed OUTPUT_FILE_PATH to OUTPUT_FILE and renamed MAPPING_FILE_PATH to MAPPING_FILE
```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
#!plaintext
Copyright (c) 2017 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/util-rdm-json-generation/src/master/QuickStart.md).


