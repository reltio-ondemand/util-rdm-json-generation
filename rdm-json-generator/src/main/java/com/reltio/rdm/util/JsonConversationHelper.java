/**
 *
 */
package com.reltio.rdm.util;


/**
 * This class is used to generate the JSON for Simple, Nested & Reference
 * Attributes
 */
public class JsonConversationHelper {

    /**
     * Checks whether the column is static Value Column
     *
     * @param inputVal
     *
     * @return true/false
     */
    public static boolean isStaticValue(String inputVal) {
        return inputVal.contains("{") && inputVal.contains("}");
    }

    /**
     * Checks whether the input column is Multi value from Normalized File
     *
     * @param inputVal
     *
     * @return
     */
    public static boolean isMultiValue(String inputVal) {
        return inputVal.startsWith("[") && inputVal.endsWith("]");
    }

    /**
     * This is the helper method to get the Column Value from whole line data
     * using the column Index
     *
     * @param columnIndex
     * @param lineValues
     *
     * @return Column Value String
     */
    private static String getColumnValue(String columnIndex, String[] lineValues) {

        String value = lineValues[Integer.parseInt(columnIndex)];

        if (checkNotNull(value)) {
            return value.trim();
        }
        return null;

    }

    /**
     * Removes the First & last character from the String to get the
     * static/Multi Value Column Index
     *
     * @param staticVal
     *
     * @return columnIndex
     */
    public static String getStaticMultiValue(String staticVal) {

        return staticVal.substring(1, staticVal.length() - 1);

    }


    /**
     * Checks whether the input value is not null
     *
     * @param value
     *
     * @return true if not null, else false
     */
    public static boolean checkNotNull(String value) {
        return value != null && !value.trim().equals("")
                && !value.trim().equals("UNKNOWN")
                && !value.trim().equals("<blank>")
                && !value.trim().equals("<UNAVAIL>")
                && !value.trim().equals("#")
                && !value.toLowerCase().trim().equals("null")
                && !value.toLowerCase().trim().equals("\"");
    }


    public static String[] splitStringByDelimiter(String value, String delimiter) {
        if (checkNotNull(value)) {
            return value.split(delimiter);
        }
        return null;
    }


}
