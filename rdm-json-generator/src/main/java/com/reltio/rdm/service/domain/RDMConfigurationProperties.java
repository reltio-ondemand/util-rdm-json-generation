package com.reltio.rdm.service.domain;

import java.io.Serializable;
import java.util.Properties;

import static com.reltio.rdm.constants.JsonGeneratorProperties.MAX_THREAD_COUNT;
import static com.reltio.rdm.constants.JsonGeneratorProperties.MIN_THREAD_COUNT;

/**
 * Created by gowganesh on 12/06/17.
 */
public class RDMConfigurationProperties implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1816716867915416966L;
	private String inputDataFilePath;
    private String inputFileFormat;
    private String inputFileDelimiter;
    private String outputFilePath;
    private String mappingFilePath;
    private Integer threadCount;


    public RDMConfigurationProperties(Properties properties) {

        // READ the Config Properties values
        inputDataFilePath = properties.getProperty("INPUT_DATA_FILE");
        inputFileFormat = properties.getProperty("INPUT_FILE_FORMAT");
        inputFileDelimiter = properties.getProperty("INPUT_FILE_DELIMITER");
        outputFilePath = properties.getProperty("OUTPUT_FILE");
        mappingFilePath = properties.getProperty("MAPPING_FILE");

        String threadCountStr = properties.getProperty("THREAD_COUNT");
        if (threadCountStr == null || threadCountStr.isEmpty()) {
            setThreadCount(null);
        } else {
            setThreadCount(Integer.parseInt(threadCountStr));
        }
    }

    public String getInputDataFilePath() {
        return inputDataFilePath;
    }

    public void setInputDataFilePath(String inputDataFilePath) {
        this.inputDataFilePath = inputDataFilePath;
    }

    public String getInputFileFormat() {
        return inputFileFormat;
    }

    public void setInputFileFormat(String inputFileFormat) {
        this.inputFileFormat = inputFileFormat;
    }

    public String getInputFileDelimiter() {
        return inputFileDelimiter;
    }

    public void setInputFileDelimiter(String inputFileDelimiter) {
        this.inputFileDelimiter = inputFileDelimiter;
    }

    public String getOutputFilePath() {
        return outputFilePath;
    }

    public void setOutputFilePath(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

    public String getMappingFilePath() {
        return mappingFilePath;
    }

    public void setMappingFilePath(String mappingFilePath) {
        this.mappingFilePath = mappingFilePath;
    }

    public Integer getThreadCount() {
        return threadCount;
    }

    /**
     * @param threadCount the threadCount to set
     */
    public void setThreadCount(Integer threadCount) {
        if (threadCount == null) {
            this.threadCount = MIN_THREAD_COUNT;
        } else if (threadCount > MAX_THREAD_COUNT) {
            this.threadCount = MAX_THREAD_COUNT;

        } else {
            this.threadCount = threadCount;
        }
    }
}
