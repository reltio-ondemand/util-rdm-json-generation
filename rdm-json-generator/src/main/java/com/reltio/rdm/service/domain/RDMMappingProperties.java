package com.reltio.rdm.service.domain;

import com.reltio.rdm.constants.JsonGeneratorProperties;
import com.reltio.rdm.domain.Attribute;
import com.reltio.rdm.domain.LocalizationValues;
import com.reltio.rdm.domain.SourceLevelValues;
import com.reltio.rdm.util.JsonConversationHelper;

import java.util.*;

/**
 * Created by gowganesh on 12/06/17.
 */
public class RDMMappingProperties {


    // Keep non-valid column details list
    private final Set<String> nonColumnValues = new HashSet<>();
    private String tenantId;
    private String type;
    private String code;
    private String canonicalEnabled;
    private String startDate;
    private String endDate;
    private String enabled;
    private String canonicalValue;
    private String downStreamDefaultValue;
    private Map<Integer, LookupSource> lookupSources = new HashMap<>();
    private Map<Integer, Localization> localizations = new HashMap<>();
    private Map<Integer, Parents> parentsData = new HashMap<>();
    private List<Attribute> attributes = new ArrayList<>();

    public RDMMappingProperties(Properties properties, Map<String, Integer> columnIndexMap) {
        Map<String, String> attributeMappingDetails = new HashMap<>((Map) properties);
        for (Map.Entry<String, String> mappingEntry : attributeMappingDetails
                .entrySet()) {

            // Get the Column Index or Static value
            String column = mappingEntry.getValue().trim();
            if (columnIndexMap.get(column) == null) {
                nonColumnValues.add(column);
            } else {
                column = columnIndexMap.get(column) + "";
            }


            // Convert the different mapping properties to expected RDM object data
            String key = mappingEntry.getKey().trim();
            if (key.equals("TENANT_ID")) {
                tenantId = column;
            } else if (key.equals("START_DATE")) {
                startDate = column;
            } else if (key.equals("END_DATE")) {
                endDate = column;
            } else if (key.equals("LOOKUP_TYPE")) {
                type = column;
            } else if (key.equals("LOOKUP_CODE_CANONICAL")) {
                code = column;
            } else if (key.equals("CANONICAL_CODE_ENABLED")) {
            	canonicalEnabled = column;
            } else if (key.startsWith("ATTRIBUTES")) {
                String[] splitValues = JsonConversationHelper.splitStringByDelimiter(key, "\\" + JsonGeneratorProperties.NESTED_ATTR_FLAG);
                if (splitValues.length > 1) {
                    Attribute attribute = new Attribute();
                    attribute.setName(splitValues[1]);
                    attribute.setValue(column);
                    attributes.add(attribute);
                }
            } else if (key.startsWith("PARENTS")) {
                String[] splitValues = JsonConversationHelper.splitStringByDelimiter(key, "\\" + JsonGeneratorProperties.NESTED_ATTR_FLAG);
                if (splitValues.length > 1) {
                    String parentKey = splitValues[0];
                    Integer indexValue = 0;
                    if (parentKey.contains(JsonGeneratorProperties.MULTI_VALUE_FLAG)) {
                        indexValue = Integer.parseInt(parentKey.replaceAll("PARENTS#", ""));
                    }
                    Parents parents = parentsData.get(indexValue);
                    if (parents == null) {
                        parents = new Parents();
                    }
                    if (Objects.equals(splitValues[1], "TYPE")) { //todo equal to operation changed from == to .equalsTo()
                        parents.setType(column);
                    } else {
                        parents.setValue(column);
                    }
                    parentsData.put(indexValue, parents);
                }
            } else if (key.startsWith("LOOKUP_SOURCE")) {

                String lookupKey = key;
                Boolean lookupNested = key.contains(JsonGeneratorProperties.NESTED_ATTR_FLAG);
                String[] splitValues = null;
                if (lookupNested) {
                    splitValues = JsonConversationHelper.splitStringByDelimiter(key, "\\" + JsonGeneratorProperties.NESTED_ATTR_FLAG);
                    lookupKey = splitValues[0];
                }

                Integer indexValue = 0;
                if (lookupKey.contains(JsonGeneratorProperties.MULTI_VALUE_FLAG)) {
                    indexValue = Integer.parseInt(lookupKey.replaceAll("LOOKUP_SOURCE#", ""));
                }
                LookupSource lookupSource = lookupSources.get(indexValue);
                if (lookupSource == null) {
                    lookupSource = new LookupSource();
                }

                if (lookupNested) {
                    String lookupInnerKey = splitValues[1].trim();
                    Integer innerIndexValue = 0;
                    if (lookupInnerKey.contains(JsonGeneratorProperties.MULTI_VALUE_FLAG)) {
                        String[] lookupInnerData = JsonConversationHelper.splitStringByDelimiter(lookupInnerKey, JsonGeneratorProperties.MULTI_VALUE_FLAG);
                        innerIndexValue = Integer.parseInt(lookupInnerData[1]);
                    }

                    SourceLevelValues sourceLevelValues = lookupSource.getLookupValues().get(innerIndexValue);
                    if (sourceLevelValues == null) {
                        sourceLevelValues = new SourceLevelValues();
                    }

                    if (lookupInnerKey.startsWith("CODE")) {
                        sourceLevelValues.setCode(column);
                    } else if (lookupInnerKey.startsWith("VALUE")) {
                        sourceLevelValues.setValue(column);
                    } else if (lookupInnerKey.startsWith("ENABLED")) {
                        sourceLevelValues.setEnabled(column);
                    } else if (lookupInnerKey.startsWith("CANONICALVALUE")) {
                        sourceLevelValues.setCanonicalValue(column);
                    } else if (lookupInnerKey.startsWith("DOWNSTREAMDEFAULTVALUE")) {
                        sourceLevelValues.setDownStreamDefaultValue(column);
                    } else if (lookupInnerKey.startsWith("DESCRIPTION")) {
                        sourceLevelValues.setDescription(column);
                    }
                    lookupSource.getLookupValues().put(innerIndexValue, sourceLevelValues);


                } else {
                    lookupSource.setSource(column);
                }

                lookupSources.put(indexValue, lookupSource);
            } else if (key.startsWith("LOCALIZATION")) {

                String lookupKey = key;
                Boolean lookupNested = key.contains(JsonGeneratorProperties.NESTED_ATTR_FLAG);
                String[] splitValues = null;
                if (lookupNested) {
                    splitValues = JsonConversationHelper.splitStringByDelimiter(key, "\\" + JsonGeneratorProperties.NESTED_ATTR_FLAG);
                    lookupKey = splitValues[0];
                }

                Integer indexValue = 0;
                if (lookupKey.contains(JsonGeneratorProperties.MULTI_VALUE_FLAG)) {
                    indexValue = Integer.parseInt(lookupKey.replaceAll("LOCALIZATION#", ""));
                }
                Localization localization = localizations.get(indexValue);
                if (localization == null) {
                    localization = new Localization();
                }

                if (lookupNested) {
                    String lookupInnerKey = splitValues[1].trim();
                    Integer innerIndexValue = 0;
                    if (lookupInnerKey.contains(JsonGeneratorProperties.MULTI_VALUE_FLAG)) {
                        String[] lookupInnerData = JsonConversationHelper.splitStringByDelimiter(lookupInnerKey, JsonGeneratorProperties.MULTI_VALUE_FLAG);
                        innerIndexValue = Integer.parseInt(lookupInnerData[1]);
                    }

                    LocalizationValues localizationValues = localization.getLocalizationValues().get(innerIndexValue);
                    if (localizationValues == null) {
                        localizationValues = new LocalizationValues();
                    }

                    if (lookupInnerKey.startsWith("LANGUAGECODE")) {
                        localizationValues.setLanguageCode(column);
                    } else if (lookupInnerKey.startsWith("VALUE")) {
                        localizationValues.setValue(column);
                    } else if (lookupInnerKey.startsWith("DESCRIPTION")) {
                        localizationValues.setDescription(column);
                    }
                    localization.getLocalizationValues().put(innerIndexValue, localizationValues);

                }

                localizations.put(indexValue, localization);
            }
        }

    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCanonicalEnabled() {
		return canonicalEnabled;
	}

	public void setCanonicalEnabled(String canonicalEnabled) {
		this.canonicalEnabled = canonicalEnabled;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getCanonicalValue() {
		return canonicalValue;
	}

	public void setCanonicalValue(String canonicalValue) {
		this.canonicalValue = canonicalValue;
	}

	public String getDownStreamDefaultValue() {
		return downStreamDefaultValue;
	}

	public void setDownStreamDefaultValue(String downStreamDefaultValue) {
		this.downStreamDefaultValue = downStreamDefaultValue;
	}

	public Map<Integer, LookupSource> getLookupSources() {
        return lookupSources;
    }

    public void setLookupSources(Map<Integer, LookupSource> lookupSources) {
        this.lookupSources = lookupSources;
    }
    
    public Map<Integer, Localization> getLocalization() {
        return localizations;
    }

    public void setLocalization(Map<Integer, Localization> localizations) {
        this.localizations = localizations;
    }

    public Map<Integer, Parents> getParentsData() {
        return parentsData;
    }

    public void setParentsData(Map<Integer, Parents> parentsData) {
        this.parentsData = parentsData;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public Set<String> getNonColumnValues() {
        return nonColumnValues;
    }

    public class LookupSource {

        private String source;

        private Map<Integer, SourceLevelValues> lookupValues = new HashMap<>();


        public Map<Integer, SourceLevelValues> getLookupValues() {
            return lookupValues;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }
    }
    
    public class Localization {
    	
    	private Map<Integer, LocalizationValues> localizationValues = new HashMap<>();
    	
    	public Map<Integer, LocalizationValues> getLocalizationValues() {
            return localizationValues;
        }
    }

    public class Parents {

        private String type;
        private String value;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
