package com.reltio.rdm.service;

import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.reltio.cst.util.Util;
import com.reltio.file.ReltioCSVFileReader;
import com.reltio.file.ReltioFileReader;
import com.reltio.file.ReltioFileWriter;
import com.reltio.file.ReltioFlatFileReader;
import com.reltio.file.ReltioFlatFileWriter;
import com.reltio.rdm.constants.JsonGeneratorProperties;
import com.reltio.rdm.domain.Attribute;
import com.reltio.rdm.domain.LocalizationValues;
import com.reltio.rdm.domain.LookupData;
import com.reltio.rdm.domain.SourceLevelMapping;
import com.reltio.rdm.domain.SourceLevelValues;
import com.reltio.rdm.service.domain.RDMConfigurationProperties;
import com.reltio.rdm.service.domain.RDMMappingProperties;
import com.reltio.rdm.util.JsonConversationHelper;

/**
 * Created by gowganesh on 12/06/17.
 */
public class RDMJsonGeneratorService {
	private static final Logger logger = LogManager.getLogger(RDMJsonGeneratorService.class.getName());

	private static Gson gson = new Gson();
	// Keep column name and index in the input file
	private final Map<String, Integer> columnIndexMap = new HashMap<>();

	public static void main(String[] args) throws Exception {
		logger.info("Process Started");
		long programStartTime = System.currentTimeMillis();
		int count = 0;
		final int[] rejectedRecordCount = new int[1];
		rejectedRecordCount[0] = 0;

		final RDMJsonGeneratorService jsonGeneratorService = new RDMJsonGeneratorService();

		Properties config = new Properties();
		FileReader fileReader = null;
		try {
			String propertyFilePath = args[0];

			config = Util.getProperties(propertyFilePath);
		} catch (Exception e) {
			logger.error("Failed Read the Properties File :: ");
			e.printStackTrace();
		} finally {
			if (fileReader != null) {
				try {
					fileReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}

		final RDMConfigurationProperties rdmConfigurationProperties = new RDMConfigurationProperties(config);

		if (args != null && args.length > 1) {
			rdmConfigurationProperties.setInputDataFilePath(args[1]);
			rdmConfigurationProperties.setMappingFilePath(args[2]);
			rdmConfigurationProperties.setOutputFilePath(args[3]);
		}

		
		List<String> missingKeys = Util.listMissingProperties(config,
				Arrays.asList("INPUT_DATA_FILE","INPUT_FILE_FORMAT","OUTPUT_FILE", "MAPPING_FILE"));

		if (!missingKeys.isEmpty()) {
			System.out.println(
					"Following properties are missing from configuration file!! \n" + String.join("\n", missingKeys));
			System.exit(0);
		}


		// Read the Mapping File configuration
		config.clear();

		try {
			fileReader = new FileReader(StringEscapeUtils.escapeJava(rdmConfigurationProperties.getMappingFilePath()));
			config.load(fileReader);

		} catch (Exception e) {
			logger.error(
					"Failed to Read the Properties File :: " + rdmConfigurationProperties.getMappingFilePath());
			e.printStackTrace();
			System.exit(-1);
		} finally {
			if (fileReader != null) {
				try {
					fileReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// Create Reader for input data File
		ReltioFileReader inputDataFileReader = null;

		if (rdmConfigurationProperties.getInputFileFormat().equals("CSV")) {
			inputDataFileReader = new ReltioCSVFileReader(rdmConfigurationProperties.getInputDataFilePath());
		} else {
			if (rdmConfigurationProperties.getInputFileDelimiter() == null) {
				inputDataFileReader = new ReltioFlatFileReader(rdmConfigurationProperties.getInputDataFilePath());
			} else {
				inputDataFileReader = new ReltioFlatFileReader(rdmConfigurationProperties.getInputDataFilePath(),
						rdmConfigurationProperties.getInputFileDelimiter());
			}
		}

		// Create Writter for outputFile
		final ReltioFileWriter reltioFileWriter = new ReltioFlatFileWriter(
				rdmConfigurationProperties.getOutputFilePath(), "UTF-8");

		String[] lineValues = null;

		// Read Header
		lineValues = inputDataFileReader.readLine();

		final int sizeOfHeaderColumn = lineValues.length;

		List<String> allColumnNames = Arrays.asList(lineValues);

		// Create a Map for Column Name to Index
		for (int i = 0; i < allColumnNames.size(); i++) {
			jsonGeneratorService.columnIndexMap.put(allColumnNames.get(i).trim(), i);
		}

		RDMMappingProperties rdmMappingProperties = new RDMMappingProperties(config,
				jsonGeneratorService.columnIndexMap);

		boolean isNonColumnValuePresent = false;

		if (!rdmMappingProperties.getNonColumnValues().isEmpty()) {

			for (String col : rdmMappingProperties.getNonColumnValues()) {
				if (!col.startsWith(JsonGeneratorProperties.STATIC_VALUE_PREFIX)
						|| !col.endsWith(JsonGeneratorProperties.STATIC_VALUE_SUFFIX)) {
					if (!isNonColumnValuePresent) {
						logger.error("Non-Column Values in the Mapping File Below::::");
					}
					logger.error(col);
					isNonColumnValuePresent = true;
				}
			}
		}

		if (isNonColumnValuePresent) {
			logger.error("Please validate the Mapping File Column Names listed above and re-try.....");
			System.exit(-1);
		}

		// Thread Operations
		ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors
				.newFixedThreadPool(rdmConfigurationProperties.getThreadCount());
		ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();
		boolean eof = false;

		List<String[]> inputLineValues = new ArrayList<>();
		while (!eof) {
			for (int threadNum = 0; threadNum < rdmConfigurationProperties.getThreadCount()
					* JsonGeneratorProperties.MAX_QUEUE_SIZE_MULTIPLICATOR; threadNum++) {
				inputLineValues.clear();

				for (int k = 0; k < JsonGeneratorProperties.GROUP_TO_SENT; k++) {

					// Read line
					lineValues = inputDataFileReader.readLine();
					if (lineValues == null) {
						eof = true;
						break;
					}
					count++;
					inputLineValues.add(lineValues);

				}

				logger.info("Number of records read from file =" + count);
				if (!inputLineValues.isEmpty()) {
					final List<String[]> threadInputLines = new ArrayList<>(inputLineValues);

					futures.add(executorService.submit(new Callable<Long>() {

						@Override
						public Long call() {
							long requestExecutionTime = 0l;
							long startTime = System.currentTimeMillis();
							try {

								List<LookupData> lookupDatas = new ArrayList<LookupData>();
								LookupData lookupData = null;

								for (String[] lineValues : threadInputLines) {
									lookupData = new LookupData();
									lookupData.setTenantId(
											getStaticOrColumnValue(rdmMappingProperties.getTenantId(), lineValues));
									lookupData.setType(
											getStaticOrColumnValue(rdmMappingProperties.getType(), lineValues));
									lookupData.setCode(
											getStaticOrColumnValue(rdmMappingProperties.getCode(), lineValues));

									if (JsonConversationHelper.checkNotNull(rdmMappingProperties.getCanonicalEnabled())) {
										lookupData.setEnabled(
												getStaticOrColumnValue(rdmMappingProperties.getCanonicalEnabled(), lineValues));
									}
									
									if (JsonConversationHelper.checkNotNull(rdmMappingProperties.getStartDate())) {
										lookupData.setStartDate(
												getStaticOrColumnValue(rdmMappingProperties.getStartDate(), lineValues));
									}
									
									if (JsonConversationHelper.checkNotNull(rdmMappingProperties.getEndDate())) {
										lookupData.setEndDate(
												getStaticOrColumnValue(rdmMappingProperties.getEndDate(), lineValues));
									}
									
									// Parents Processing
									for (RDMMappingProperties.Parents parents : rdmMappingProperties.getParentsData()
											.values()) {
										String value = getStaticOrColumnValue(parents.getValue(), lineValues);
										String type = getStaticOrColumnValue(parents.getType(), lineValues);// added
																											// line
										if (JsonConversationHelper.checkNotNull(value)
												&& JsonConversationHelper.checkNotNull(type)) {
											lookupData.getParents().add(type + value);
										}
									}

									// Attributes Processing
									for (Attribute attribute : rdmMappingProperties.getAttributes()) {
										String value = getStaticOrColumnValue(attribute.getValue(), lineValues);
										if (JsonConversationHelper.checkNotNull(value)
												&& JsonConversationHelper.checkNotNull(attribute.getName())) {
											Attribute attribute1 = new Attribute();
											attribute1.setName(attribute.getName());
											attribute1.setValue(value);
											lookupData.getAttributes().add(attribute1);
										}
									}

									// Lookup Source Processing
									for (RDMMappingProperties.LookupSource lookupSource : rdmMappingProperties
											.getLookupSources().values()) {
										String value = getStaticOrColumnValue(lookupSource.getSource(), lineValues);

										if (JsonConversationHelper.checkNotNull(value)) {

											SourceLevelMapping sourceLevelMapping = resolveMapping(
													lookupData.getSourceMappings(), value);

											sourceLevelMapping.setSource(value);

											for (SourceLevelValues sourceLevelValues : lookupSource.getLookupValues()
													.values()) {

												String code = getStaticOrColumnValue(sourceLevelValues.getCode(),
														lineValues);
												String codeValue = getStaticOrColumnValue(sourceLevelValues.getValue(),
														lineValues);

												if (JsonConversationHelper.checkNotNull(code)
														&& JsonConversationHelper.checkNotNull(codeValue)) {

													SourceLevelValues sourceLevelValues1 = new SourceLevelValues();
													sourceLevelValues1.setCode(code);
													sourceLevelValues1.setValue(codeValue);
													
													if (JsonConversationHelper.checkNotNull(sourceLevelValues.getEnabled())) {
														String enabled = getStaticOrColumnValue(sourceLevelValues.getEnabled(), lineValues);
														sourceLevelValues1.setEnabled(enabled);
													}
													
													if (JsonConversationHelper.checkNotNull(sourceLevelValues.getCanonicalValue())) {
														String canonicalValue = getStaticOrColumnValue(sourceLevelValues.getCanonicalValue(), lineValues);
														sourceLevelValues1.setCanonicalValue(canonicalValue);
													}
													
													if (JsonConversationHelper.checkNotNull(sourceLevelValues.getDownStreamDefaultValue())) {
														String downStreamDefaultValue = getStaticOrColumnValue(sourceLevelValues.getDownStreamDefaultValue(), lineValues);
														sourceLevelValues1.setDownStreamDefaultValue(downStreamDefaultValue);
													}
													
													if (JsonConversationHelper.checkNotNull(sourceLevelValues.getDescription())) {
														String description = getStaticOrColumnValue(sourceLevelValues.getDescription(), lineValues);
														sourceLevelValues1.setDescription(description);
													}

													sourceLevelMapping.getValues().add(sourceLevelValues1);
												}

											}

											updateLookUpdata(lookupData, sourceLevelMapping);

										}
									}
									
									
									// Lookup Localizations Processing
									for (RDMMappingProperties.Localization localization : rdmMappingProperties
											.getLocalization().values()) {
										
										LocalizationValues localizationValue1 = new LocalizationValues();

											for (LocalizationValues localizationValue : localization.getLocalizationValues().values()) {
													
													if (JsonConversationHelper.checkNotNull(localizationValue.getLanguageCode())) {
														String langCode = getStaticOrColumnValue(localizationValue.getLanguageCode(), lineValues);
														localizationValue1.setLanguageCode(langCode);
													}
													
													if (JsonConversationHelper.checkNotNull(localizationValue.getValue())) {
														String locValue = getStaticOrColumnValue(localizationValue.getValue(), lineValues);
														localizationValue1.setValue(locValue);
													}
													
													if (JsonConversationHelper.checkNotNull(localizationValue.getDescription())) {
														String locDescription = getStaticOrColumnValue(localizationValue.getDescription(), lineValues);
														localizationValue1.setDescription(locDescription);
													}

											}

											lookupData.getLocalizations().add(localizationValue1);

									}

									lookupDatas.add(lookupData);
									reltioFileWriter.writeToFile(gson.toJson(lookupDatas));
									lookupDatas.clear();
								}

							} catch (Exception e) {
								logger.error("Unexpected Error happened .... " + e.getMessage());
								e.printStackTrace();
							}
							requestExecutionTime = System.currentTimeMillis() - startTime;
							return requestExecutionTime;
						}

						/**
						 * @param lookupData
						 * @param sourceLevelMapping
						 * 
						 *            Addes to the LookUpData only when the Source of SourceLevelMapping
						 *            doesnot exist.
						 */
						private void updateLookUpdata(LookupData lookupData, SourceLevelMapping sourceLevelMapping) {

							boolean isExist = false;

							for (SourceLevelMapping sourceL : lookupData.getSourceMappings()) {
								if (sourceLevelMapping.getSource().equals(sourceL.getSource())) {
									isExist = true;
									break;
								}
							}

							if (!isExist) {

								lookupData.getSourceMappings().add(sourceLevelMapping);
							}

						}

						/**
						 * @param sourceMappings
						 * @param code
						 * @return
						 * 
						 * 		Checks for the same source, returns that SourceLevelMapping and
						 *         creates new SourceLevelMapping for different sources.
						 */
						private SourceLevelMapping resolveMapping(List<SourceLevelMapping> sourceMappings,
								String code) {

							SourceLevelMapping result = new SourceLevelMapping();

							for (SourceLevelMapping map : sourceMappings) {
								if (code.equals(map.getSource())) {
									result = map;
									break;
								}
							}

							return result;
						}
					}));

				}

				if (eof) {
					break;
				}
			}

			logger.debug("Records processed =" + count);
			waitForTasksReady(futures, rdmConfigurationProperties.getThreadCount()
					* (JsonGeneratorProperties.MAX_QUEUE_SIZE_MULTIPLICATOR / 2));
		}

		waitForTasksReady(futures, 0);

		reltioFileWriter.close();
		inputDataFileReader.close();
		logger.debug("\n \n *** Final Metrics of the JSON Generation ***");

		logger.debug("Total Number of Records in the File = " + count);
		logger.debug("Total Number of Rejected Records = " + rejectedRecordCount[0]);
		logger.debug("Total Number of records processed sucessfully = " + (count - rejectedRecordCount[0]));
		logger.debug("Total Time Taken in (ms) = " + (System.currentTimeMillis() - programStartTime));
		executorService.shutdown();
		logger.debug("Process Completed");

	}

	/**
	 * Waits for futures (load tasks list put to executor) are partially ready.
	 * <code>maxNumberInList</code> parameters specifies how much tasks could be
	 * uncompleted.
	 *
	 * @param futures
	 *            - futures to wait for.
	 * @param maxNumberInList
	 *            - maximum number of futures could be left in "undone" state.
	 *
	 * @return sum of executed futures execution time.
	 */
	public static long waitForTasksReady(Collection<Future<Long>> futures, int maxNumberInList) {
		long totalResult = 0l;
		while (futures.size() > maxNumberInList) {
			try {
				Thread.sleep(20);
			} catch (Exception e) {
				// todo need to update with the exception..
			}
			for (Future<Long> future : new ArrayList<Future<Long>>(futures)) {
				if (future.isDone()) {
					try {
						totalResult += future.get();
						futures.remove(future);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return totalResult;
	}

	public static String getStaticOrColumnValue(String input, String[] lineValues) {
		// Check whether it is a Static Value
		if (JsonConversationHelper.isStaticValue(input)) {
			return JsonConversationHelper.getStaticMultiValue(input);
		} else {
			// If it is a single value column then get the value for that Column
			return getColumnValue(input, lineValues);
		}
	}

	/**
	 * This is the helper method to get the Column Value from whole line data using
	 * the column Index
	 *
	 * @param columnIndex
	 * @param lineValues
	 *
	 * @return Column Value String
	 */
	private static String getColumnValue(String columnIndex, String[] lineValues) {
		try {
			if (lineValues.length > Integer.parseInt(columnIndex)) {
				String value = lineValues[Integer.parseInt(columnIndex)];
				if (JsonConversationHelper.checkNotNull(value)) {
					return value.trim();
				}
			}
		} catch (Exception e) {
			logger.error("Exception ::: " + e.getMessage() + "Returning Empty");
			return null;
		}
		return null;

	}
}
