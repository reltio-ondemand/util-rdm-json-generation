package com.reltio.rdm.constants;

/**
 * Created by gowganesh on 12/06/17.
 */
public class JsonGeneratorProperties {

    public static final String NESTED_ATTR_FLAG = ".";
    public static final String REF_ATTR_FLAG = "|";
    public static final String MULTI_VALUE_FLAG = "#";
    public static final Integer MAX_THREAD_COUNT = 20;
    public static final Integer MIN_THREAD_COUNT = 5;
    public static final Integer GROUP_TO_SENT = 100;
    public static final String STATIC_VALUE_PREFIX = "{";
    public static final String STATIC_VALUE_SUFFIX = "}";
    public static final Integer MAX_QUEUE_SIZE_MULTIPLICATOR = 10;

}
