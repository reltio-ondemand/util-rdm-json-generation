package com.reltio.rdm.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mallikarjuna on 09/13/21.
 */
public class LocalizationValues {

    private String languageCode;
    
    private String value;
    
    private String description;

	public String getLanguageCode() {
		return languageCode;
	}

	public String getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
