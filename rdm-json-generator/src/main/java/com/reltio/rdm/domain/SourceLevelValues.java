package com.reltio.rdm.domain;

/**
 * Created by gowganesh on 31/05/17.
 */
public class SourceLevelValues {


    private String code;

    private String value;
    private String enabled;
    private String canonicalValue;
    private String downStreamDefaultValue;
    private String description;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getCanonicalValue() {
        return canonicalValue;
    }

    public void setCanonicalValue(String canonicalValue) {
        this.canonicalValue = canonicalValue;
    }

    public String getDownStreamDefaultValue() {
        return downStreamDefaultValue;
    }

    public void setDownStreamDefaultValue(String downStreamDefaultValue) {
        this.downStreamDefaultValue = downStreamDefaultValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
