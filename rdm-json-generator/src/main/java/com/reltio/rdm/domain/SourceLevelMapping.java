package com.reltio.rdm.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gowganesh on 31/05/17.
 */
public class SourceLevelMapping {

    private String source;

    private List<SourceLevelValues> values = new ArrayList<>();


    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public List<SourceLevelValues> getValues() {
        return values;
    }

    public void setValues(List<SourceLevelValues> values) {
        this.values = values;
    }
}
