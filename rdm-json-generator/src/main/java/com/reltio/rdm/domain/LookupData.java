package com.reltio.rdm.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gowganesh on 31/05/17.
 */
public class LookupData {


    private String tenantId;

    private String enabled;
    private String code;
    private String type;
    private String startDate;
    private String endDate;

    private List<SourceLevelMapping> sourceMappings = new ArrayList<>();
    private List<LocalizationValues> localizations = new ArrayList<>();
    private List<String> parents = new ArrayList<>();
    private List<Attribute> attributes = new ArrayList<>();


    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public List<SourceLevelMapping> getSourceMappings() {
        return sourceMappings;
    }

    public void setSourceMappings(List<SourceLevelMapping> sourceMappings) {
        this.sourceMappings = sourceMappings;
    }

    public List<LocalizationValues> getLocalizations() {
		return localizations;
	}

	public void setLocalizations(List<LocalizationValues> localizations) {
		this.localizations = localizations;
	}

	public List<String> getParents() {
        return parents;
    }

    public void setParents(List<String> parents) {
        this.parents = parents;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }
}
