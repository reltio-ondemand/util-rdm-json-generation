package com.reltio.rdm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

import static java.nio.charset.Charset.defaultCharset;

/**
 * Created by vignesh on 6/13/17.
 */
public class RDMJsonGeneratorServiceTest {
    @Test
    public void rdmJsonGeneratorTest1() throws Exception {

        String[] filePath = {new File(getClass()
                .getClassLoader()
                .getResource("properties/config-1.properties").toURI()).getAbsolutePath()};
        RDMJsonGeneratorService.main(filePath);
        ObjectMapper mapper = new ObjectMapper();

        String expectedString = Files.toString(new File(getClass()
                .getClassLoader()
                .getResource("expected-rdm-json-lookup-1.json").toURI()), defaultCharset());
        Object expectedJson = mapper.readValue(expectedString, Object.class);
        String input = Files.toString(new File(getClass()
                .getClassLoader()
                .getResource("tgt/rdm-json-lookup-1.json").toURI()), defaultCharset());
        Object inputJson = mapper.readValue(input, Object.class);

        Assert.assertEquals(inputJson, expectedJson);


    }

    @Test
    public void rdmJsonGeneratorTest2() throws Exception {

        String[] filePath = {new File(getClass()
                .getClassLoader()
                .getResource("properties/config-2.properties").toURI()).getAbsolutePath()};
        RDMJsonGeneratorService.main(filePath);
        ObjectMapper mapper = new ObjectMapper();

        String expectedString = Files.toString(new File(getClass()
                .getClassLoader()
                .getResource("expected-rdm-json-lookup-2.json").toURI()), defaultCharset());
        Object expectedJson = mapper.readValue(expectedString, Object.class);
        String input = Files.toString(new File(getClass()
                .getClassLoader()
                .getResource("tgt/rdm-json-lookup-2.json").toURI()), defaultCharset());
        Object inputJson = mapper.readValue(input, Object.class);

        Assert.assertEquals(inputJson, expectedJson);


    }
    
    @Test
    public void rdmJsonGeneratorTest3() throws Exception {

        String[] filePath = {new File(getClass()
                .getClassLoader()
                .getResource("properties/config-3.properties").toURI()).getAbsolutePath()};
        RDMJsonGeneratorService.main(filePath);
        ObjectMapper mapper = new ObjectMapper();

        String expectedString = Files.toString(new File(getClass()
                .getClassLoader()
                .getResource("expected-rdm-json-lookup-3.json").toURI()), defaultCharset());
        Object expectedJson = mapper.readValue(expectedString, Object.class);
        String input = Files.toString(new File(getClass()
                .getClassLoader()
                .getResource("tgt/rdm-json-lookup-3.json").toURI()), defaultCharset());
        Object inputJson = mapper.readValue(input, Object.class);

        Assert.assertEquals(inputJson, expectedJson);


    }

}